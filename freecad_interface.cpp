#include <iostream>
#include <string>
#include <MoFEM.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/filesystem.hpp>

using namespace MoFEM;
using namespace std;

static char help[] = "...\n\n";

// param file values
PetscBool get_modules = PETSC_FALSE;
PetscBool set_get_modules = PETSC_FALSE;
PetscBool set_set_module = PETSC_FALSE;
PetscInt mod;
PetscBool set_get_bc = PETSC_FALSE;
PetscBool get_bc = PETSC_FALSE;
PetscBool set_run = PETSC_FALSE;
char run_params[256];
size_t mod_size = sizeof run_params;

struct um {
  boost::filesystem::path execPath;
  boost::filesystem::path jsonPath;
  um(boost::filesystem::path a, boost::filesystem::path b) {
    execPath = a;
    jsonPath = b;
  }
};

// modules

MoFEMErrorCode SetParamValues() {
  MoFEMFunctionBegin;

  CHKERR PetscOptionsBegin(PETSC_COMM_WORLD, "", "FreeCAD Interface", "none");
  // Set values from param file
  CHKERR PetscOptionsBool("-get_modules", "show list of supported modules", "",
                          PETSC_FALSE, &get_modules, &set_get_modules);
  CHKERR PetscOptionsInt("-module", "set the current work module", "", 0, &mod,
                         &set_set_module);
  CHKERR PetscOptionsBool("-bc", "returns the modules bcs", "", PETSC_FALSE,
                          &get_bc, &set_get_bc);
  CHKERR PetscOptionsString("-run", "runs the module with the given params", "",
                            "", run_params, mod_size, &set_run);
  ierr = PetscOptionsEnd();
  CHKERRQ(ierr);

  MoFEMFunctionReturn(0);
}

// Checks if a json is valid for mofem use
bool IsValidJSON(string path) {
  boost::property_tree::ptree root;
  boost::property_tree::read_json(path, root);
  return root.get<bool>("MoFEM");
}

// Gets the directories for all of the installed modules
// Validates by checking if each folder contains a json file with the property
// MoFEM in it

vector<um> GetModuleDirs(boost::filesystem::path pp) {
  vector<um> module_dirs = {};
  pp = pp.parent_path();
  boost::filesystem::path jsonPath;
  boost::filesystem::path execPath;
  for (boost::filesystem::directory_entry &entry :
       boost::filesystem::directory_iterator(pp)) {
    if (is_directory(entry)) {
      jsonPath.clear();
      execPath.clear();
      for (boost::filesystem::directory_entry &x :
           boost::filesystem::directory_iterator(entry)) {
        if (boost::filesystem::extension(x.path()).compare(".json") == 0 &&
            IsValidJSON(x.path().string())) {
          jsonPath = x.path();
        } else if (x.path().stem().string().find("mofem") == string::npos) {
          execPath = x.path();
        }
        if (!jsonPath.empty() && !execPath.empty()) {
          um newUm(execPath, jsonPath);
          module_dirs.push_back(newUm);
          jsonPath.clear();
          execPath.clear();
        }
      }
    }
  }
  return module_dirs;
}

void ParamStack(vector<um> &dirs) {
  const int module_len = dirs.size();

  CHKERR SetParamValues();
  if (set_get_modules) {
    for (int i = 0; i < module_len; i++) {
      cout << boost::format("%i %s") % i %
                  dirs[i].execPath.parent_path().filename().string()
           << endl;
    }
  }
  if (set_set_module) {
    if (mod < 0 || mod > module_len) {
      MOFEM_LOG("MYCHANNEL", Sev::error) << "Incorrect module inputted" << endl;
      return;
    }
  }
  if (set_get_bc && get_bc) {
    if (!set_set_module) {
      MOFEM_LOG("MYCHANNEL", Sev::error)
          << "You need to set a module before you can get its boundary "
             "conditions"
          << endl;
      return;
    }
    if (!boost::filesystem::exists(dirs[mod].jsonPath)) {
      MOFEM_LOG("MYCHANNEL", Sev::error) << "Cannot find file" << std::endl;
      return;
    }
    cout << boost::filesystem::absolute(dirs[mod].jsonPath).string() << endl;
  }
  if (set_run) {
    if (!set_set_module) {
      MOFEM_LOG("MYCHANNEL", Sev::error)
          << "You need to set a module before you can run an analysis" << endl;
      return;
    }
    cout << dirs[mod].execPath.string() << endl;
  }
}

void RunModule(vector<um> &dirs) {}

int main(int argc, char *argv[]) {
  vector<um> dirs =
      GetModuleDirs(boost::filesystem::system_complete(argv[0]).parent_path());

  // Set MoFEM messages severity to warning
  PetscOptionsInsertString(NULL, " -log_sl warning ");

  // Initialisation of MoFEM/PETSc and MOAB data structures
  // const char param_file[] = "param_file.petsc";
  MoFEM::Core::Initialize(&argc, &argv, "", help);
  auto core_log = logging::core::get();
  core_log->add_sink(
      LogManager::createSink(LogManager::getStrmWorld(), "MYCHANNEL"));
  LogManager::setLog("MYCHANNEL");

  try {
    ParamStack(dirs);
  }
  CATCH_ERRORS;
  MoFEM::Core::Finalize();

  return 0;
}