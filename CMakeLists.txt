# MoFEM is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your
# option) any later version.
#
# MoFEM is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
# License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with MoFEM. If not, see <http://www.gnu.org/licenses/>

include_directories(${CMAKE_CURRENT_SOURCE_DIR})

function(dd_build_and_install target source) # create function dd_build_and_install
  add_executable(${target} ${source}) # 
  target_link_libraries(${target}
    users_modules
    mofem_finite_elements
    mofem_interfaces
    mofem_multi_indices
    mofem_petsc
    mofem_approx
    mofem_third_party
    ${MoFEM_PROJECT_LIBS})
  install(TARGETS ${target} DESTINATION ${MODULE_NAME})
endfunction(dd_build_and_install)

dd_build_and_install(
  mofem_freecad_interface # executable name
  ${CMAKE_CURRENT_SOURCE_DIR}/freecad_interface.cpp) # cpp file for exe


set(permissions_default)


# Add all json files here
configure_file("linear.json" "${permissions_default}")